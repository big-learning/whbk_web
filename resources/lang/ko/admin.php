<?php

return [
    'online'                => 'Online',
    'login'                 => '로그인',
    'logout'                => '로그아웃',
    'setting'               => '세팅',
    'name'                  => '이름',
    'username'              => '유저아이디',
    'password'              => '비밀번호',
    'password_confirmation' => '비밀번호 확인',
    'remember_me'           => '유저 정보 저장',
    'user_setting'          => 'User setting',
    'avatar'                => 'Avatar',

    'list'         => '리스트',
    'new'          => '생성',
    'create'       => '생성',
    'delete'       => '삭제',
    'remove'       => '이동',
    'edit'         => '수정',
    'view'         => '보기',
    'browse'       => '브라우저',
    'reset'        => '원상태로',
    'export'       => '내보내기',
    'batch_delete' => '배치 삭제',
    'save'         => '저장',
    'refresh'      => '새로고침',
    'order'        => 'Order',
    'expand'       => 'Expand',
    'collapse'     => 'Collapse',
    'filter'       => '필터',
    'close'        => '닫기',
    'show'         => '보기',
    'entries'      => '개',
    'captcha'      => 'Captcha',

    'action'            => '실행',
    'title'             => '제목',
    'description'       => '설명',
    'back'              => '뒤로',
    'back_to_list'      => '리스트로',
    'submit'            => '확인',
    'menu'              => '메뉴',
    'input'             => '입력',
    'succeeded'         => '성공했습니다.',
    'failed'            => '실패했습니다.',
    'delete_confirm'    => '해당 아이템을 삭제하시겠습니까?',
    'delete_succeeded'  => '삭제가 완료되었습니다.',
    'delete_failed'     => '삭제가 실패하였습니다.',
    'update_succeeded'  => '수정을 성공하였습니다.',
    'save_succeeded'    => '저장을 성공하였습니다.',
    'refresh_succeeded' => '새로고침이 성공하였습니다.',
    'login_successful'  => '로그인을 성공하였습니다.',

    'choose'       => '선택',
    'choose_file'  => '파일 선택',
    'choose_image' => '이미지 선택',

    'more' => '더 보기',
    'deny' => '접근 차단',

    'administrator' => '관리자',
    'roles'         => '규칙',
    'permissions'   => '권한',
    'slug'          => '권한 이름',

    'created_at' => '생성날짜',
    'updated_at' => '수정날짜',

    'alert' => '팝업창',

    'parent_id' => '부모',
    'icon'      => '아이콘',
    'uri'       => 'URI',

    'operation_log'       => 'Operation log',
    'parent_select_error' => 'Parent select error',

    'pagination' => [
        'range' => ' :first 페이지 부터 :last 페이지 / 총 :total 개',
    ],

    'role'       => '규칙',
    'permission' => '권한',
    'route'      => '라우트',
    'confirm'    => '승인',
    'cancel'     => '취소',

    'http' => [
        'method' => 'HTTP 메소드',
        'path'   => 'HTTP 경로',
    ],

    'all'           => '전체',
    'current_page'  => '현재 페이지',
    'selected_rows' => '선택된 아이템',

    'upload'        => '업로드',
    'new_folder'    => '새로운 폴더',
    'time'          => '시간',
    'size'          => '크기',
];
