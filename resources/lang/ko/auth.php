<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'failed' => '이 자격증명과 일치하는 데이터가 없습니다.',
    'throttle' => '로그인 시도가 너무 많습니다. :seconds초 이후에 다시 시도하십시오.',
];