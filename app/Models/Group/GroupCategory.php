<?php

namespace App\Models\Group;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class GroupCategory extends Model
{

    protected $table = 'SG_GroupCategory';

    protected $primaryKey = 'GroupCateIdx';
    protected $fillable = [
        'GroupCateParent', 'GroupCateLevel', 'GroupCateName', 'GroupCateDescription', 'UpdateDatetime', 'CreateDatetime'
    ];

    const CREATED_AT = 'UpdateDatetime';
    const UPDATED_AT = 'CreateDatetime';





}
