<?php

namespace App\Models\Group;

use Illuminate\Database\Eloquent\Model;


class GroupUser extends Model
{

    protected $table = 'SG_GroupUser';

    protected $primaryKey = 'GroupUserIdx';
    protected $fillable = [
        'GroupIdx', 'UserIdx','GroupUserLevelIdx', 'GroupUserNickname', 'GroupUserDescription', 'GroupUserIsAdmin'
    ];

    const CREATED_AT = 'UpdateDatetime';
    const UPDATED_AT = 'CreateDatetime';



}
