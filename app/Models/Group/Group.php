<?php

namespace App\Models\Group;

use Illuminate\Database\Eloquent\Model;


class Group extends Model
{

    protected $table = 'SG_Group';

    protected $primaryKey = 'GroupIdx';
    protected $fillable = [
        'GroupCateIdx', 'GroupLocation', 'GroupStatus', 'GroupName', 'GroupDescription', 'GroupLimitMembers','GroupStartDatetime','GroupPeriod', 'UserIdx'
    ];

    const CREATED_AT = 'UpdateDatetime';
    const UPDATED_AT = 'CreateDatetime';

}
