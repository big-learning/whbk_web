<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;


class Member extends Authenticatable
{

    use Notifiable, HasApiTokens;

    protected $table = 'M_User';

    protected $primaryKey = 'UserIdx';
    protected $fillable = [
        'Email', 'Password', 'Name', 'UserType', 'UserLevel'
    ];

    protected $hidden = [
        'Password','RememberToken'
    ];


    const CREATED_AT = 'UpdateDatetime';
    const UPDATED_AT = 'CreateDatetime';


    public function findForPassport($username) {
        return $this->where('Email', $username)->first();
    }


    public function validateForPassportPasswordGrant($password)
    {
        return password_verify($password, $this->Password);
    }

}
