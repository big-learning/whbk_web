<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class MemberGroupCategory extends Model
{

    protected $table = 'M_UserGroupCategory';

    //protected $primaryKey = ['UserIdx','GroupCateIdx'];
    protected $fillable = [
        'GroupCateIdx','UserIdx'
    ];

    public $timestamps = false;

    const CREATED_AT = 'CreateDatetime';
    //const UPDATED_AT = 'UpdateDatetime';

}
