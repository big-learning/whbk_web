<?php

namespace App\Models\Board;

use Illuminate\Database\Eloquent\Model;

class ReplyOption extends Model
{
    //
    protected $table = 'CM_ReplyOption';

    protected $primaryKey = 'ReplyIdx';
    protected $fillable = [
        'ReplyBest', 'ReplyHide',  'TMP1', 'TMP2', 'TMP3'
    ];

    const CREATED_AT = 'UpdateDatetime';
    const UPDATED_AT = 'CreateDatetime';
}
