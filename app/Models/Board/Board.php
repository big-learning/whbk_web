<?php

namespace App\Models\Board;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    //
    protected $table = 'CM_Board';

    protected $primaryKey = 'BoardIdx';
    protected $fillable = [
        'BoardGroupIdx', 'BoardParentIdx', 'BoardDept', 'BoardOptionIdx', 'BoardTitle', 'BoardContent', 'UserIdx', 'BoardHit'
    ];

    const CREATED_AT = 'UpdateDatetime';
    const UPDATED_AT = 'CreateDatetime';
}
