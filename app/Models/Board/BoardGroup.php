<?php

namespace App\Models\Board;

use Illuminate\Database\Eloquent\Model;

class BoardGroup extends Model
{
    //
    protected $table = 'CM_BoardGroup';


    //public $incrementing = false;

    protected $primaryKey = 'BoardGroupIdx';
    protected $fillable = [
        'BoardGroupId', 'BoardGroupName', 'BoardGroupCate', 'BoardGroupDescription', 'BoardGroupType'
    ];

    const CREATED_AT = 'UpdateDatetime';
    const UPDATED_AT = 'CreateDatetime';
}
