<?php

namespace App\Models\Board;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    //
    protected $table = 'CM_Reply';

    protected $primaryKey = 'ReplyIdx';
    protected $fillable = [
        'BoardIdx', 'ReplyContent', 'UserIdx'
    ];

    const CREATED_AT = 'UpdateDatetime';
    const UPDATED_AT = 'CreateDatetime';
}
