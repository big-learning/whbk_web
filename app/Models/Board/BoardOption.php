<?php

namespace App\Models\Board;

use Illuminate\Database\Eloquent\Model;

class BoardOption extends Model
{
    //
    protected $table = 'CM_BoardOption';

    protected $primaryKey = 'BoardIdx';
    protected $fillable = [
        'BoardNotice', 'BoardBest', 'BoardHide', 'TMP1', 'TMP2', 'TMP3'
    ];

    const CREATED_AT = 'UpdateDatetime';
    const UPDATED_AT = 'CreateDatetime';
}
