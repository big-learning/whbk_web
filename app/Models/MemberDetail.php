<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class MemberDetail extends Model
{


    protected $table = 'M_UserDetail';

    protected $primaryKey = 'UserIdx';
    protected $fillable = [
        'UserIdx', 'Sex', 'Tel', 'Birth', 'Location', 'Info', 'FileIdx'
    ];

    public $incrementing = false;
    public $timestamps = false;


}
