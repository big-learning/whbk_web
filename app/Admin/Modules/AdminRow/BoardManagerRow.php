<?php


namespace App\Admin\Modules\AdminRow;

use Encore\Admin\Admin;

/*
 * 게시판 리스트 Action 기능 정의
 * */
class BoardManagerRow
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    protected function script()
    {
        return <<<SCRIPT

$('.grid-check-row').on('click', function () {

    console.log($(this).data('id'));

});

SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());

        return "<a class='btn btn-xs btn-success fa fa-check grid-check-row' data-id='{$this->id}'>수정</a>";
    }

    public function __toString()
    {
        return $this->render();
    }



}