<?php

namespace App\Admin\Controllers;

use App\Models\Member;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class UserController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('가입유저 정보');
            $content->description('우히부카 회원 정보입니다.');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Member::class, function (Grid $grid) {

            $grid->column('UserIdx','유저키')->sortable();
            $grid->column('UserLevel','유저등급')->display(function(){
                return config('web.LEVEL')[$this->UserLevel];
            });

            $grid->column('UserType','유저타입')->display(function(){
                return config('web.TYPE')[$this->UserType];
            });

            $grid->column('Email','이메일');

            $grid->column('Name','이름');

            $grid->column('LastDatetime','최종로그인')->display(function () {
                return $this->LastDatetime ? date('Y년 m월 d일 H시 i분', strtotime($this->LastDatetime)) : '';
            });

            $grid->column('CreateDatetime','가입날짜')->display(function () {
                return $this->CreateDatetime ? date('Y년 m월 d일 H시 i분', strtotime($this->CreateDatetime)) : '';
            });

            $grid->column('UpdateDatetime','수정날짜')->display(function () {
                return $this->UpdateDatetime ? date('Y년 m월 d일 H시 i분', strtotime($this->UpdateDatetime)) : '';
            });

            $grid->model()->orderBy('UserIdx', 'desc');


            $grid->filter(function($filter){

                $filter->like('Email', '이메일');
                $filter->equal('UserType', '유저타입')->select( config('web.TYPE'));

            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Member::class, function (Form $form) {

            $form->display('UserIdx', 'UserIdx');
            $form->display('UserLevel', 'UserLevel');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
