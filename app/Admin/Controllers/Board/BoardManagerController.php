<?php

namespace App\Admin\Controllers\Board;


use App\Admin\Modules\AdminRow\BoardManagerRow;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Board\BoardGroup;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class BoardManagerController extends Controller
{
    use ModelForm;



    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('게시판 그룹 관리');
            $content->description('게시판 그룹 관리패널입니다.');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('게시판 그룹 관리');
            $content->description('게시판 그룹 관리패널입니다.');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('게시판 그룹 관리');
            $content->description('게시판 그룹 관리패널입니다.');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(BoardGroup::class, function (Grid $grid) {

            $grid->column('BoardGroupId','게시판그룹 ID')->sortable();
            $grid->column('BoardGroupName','게시판그룹 이름');

            $grid->column('BoardGroupCate','게시판 카테고리')->display(function () {
                return config('board.group')[$this->BoardGroupCate];
            });

            $grid->column('BoardGroupType','게시판 종류')->display(function () {
                return config('board.type')[$this->BoardGroupType];
            });

            $grid->column('BoardTotalRow', '게시글 수');
            $grid->column('CreateDatetime','생성날짜')->display(function () {
                return $this->CreateDatetime ? date('Y년 m월 d일 H시 i분', strtotime($this->CreateDatetime)) : '';
            });

            $grid->column('UpdateDatetime','수정날짜')->display(function () {
                return $this->UpdateDatetime ? date('Y년 m월 d일 H시 i분', strtotime($this->UpdateDatetime)) : '';
            });


            $grid->model()->orderBy('UpdateDatetime', 'desc');


            $grid->filter(function($filter){

                $filter->like('BoardGroupName', '이름');
                $filter->like('BoardGroupCate', '카테고리');

            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(BoardGroup::class, function (Form $form) {

            $form->display("BoardGroupIdx", '게시판 IDX');
            $form->text("BoardGroupId", '게시판 ID (영문)')->rules('required|max:32|unique:CM_BoardGroup|alpha');
            $form->text('BoardGroupName', '게시판 이름')->rules('required');

            $form->select('BoardGroupCate', '게시판 그룹')->options(config('board.group'))->rules('required');
            $form->select('BoardGroupType', '게시판 종류')->options(config('board.type'))->rules('required');
            $form->textarea('BoardGroupDescription', '게시판 설명')->rules('required');
            
            //$form->display('BoardTotalRow', '전체ROW 수');

            $form->display('CreateDatetime', '생성날짜');
            $form->display('UpdateDatetime', '수정날짜');


            $form->saved(function (Form $form) {
                $this->CreateBoard($form->BoardGroupId);
            });

        });
    }


    private function CreateBoard($BoardGroupId){

        Schema::create('CM_Board__'.$BoardGroupId, function (Blueprint $table) {

            $table->increments('BoardIdx');

            $table->integer('BoardParentIdx')->unsigned();
            $table->integer('BoardDept')->unsigned();

            $table->tinyInteger('BoardNotice')->default(0);
            $table->tinyInteger('BoardBest')->default(0);
            $table->tinyInteger('BoardHide')->default(0);

            $table->string('BoardTitle',255);
            $table->text('BoardContent');

            $table->integer('UserIdx')->unsigned();
            $table->integer('BoardHit')->unsigned();

            $table->string('TMP1',128);
            $table->string('TMP2',128);
            $table->string('TMP3',128);
            $table->string('TMP4',128);
            $table->string('TMP5',128);

            $table->timestamp('UpdateDatetime')->nullable();
            $table->timestamp('CreateDatetime')->nullable();
        });


        Schema::create('CM_Reply__'.$BoardGroupId, function (Blueprint $table) {

            $table->increments('ReplyIdx');

            //$table->string('BoardGroupId',32);
            $table->integer('BoardIdx')->unsigned();

            $table->tinyInteger('ReplyBest')->default(0);
            $table->tinyInteger('ReplyHide')->default(0);

            $table->text('ReplyContent');

            $table->string('TMP1',128);
            $table->string('TMP2',128);
            $table->string('TMP3',128);
            $table->string('TMP4',128);
            $table->string('TMP5',128);

            $table->integer('UserIdx')->unsigned();
            $table->timestamp('UpdateDatetime')->nullable();
            $table->timestamp('CreateDatetime')->nullable();
        });
    }

    /*
     *  게시판 그룹 삭제
     * */
    protected function destroy($BoardGroupId){

        $boardGroup = BoardGroup::find($BoardGroupId);
        $result = $boardGroup->delete();


        if($result === false)  return response(['massage'=>'삭제가 실패하였습니다.', 'status'=> 'fail']);

        if (Schema::hasTable('CM_Reply__'.$BoardGroupId)) {
            Schema::drop('CM_Reply__' . $BoardGroupId);
        }

        if (Schema::hasTable('CM_Board__'.$BoardGroupId)) {
            Schema::drop('CM_Board__' . $BoardGroupId);
        }

        return response(['massage'=>'삭제가 완료되었습니다.', 'status'=> 'success']);
    }

}
