<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthMemberProvider extends EloquentUserProvider
{


    public function __construct(HasherContract $hasher, $model)
    {
        parent::__construct($hasher, $model);
    }

    public function validateCredentials(UserContract $user, array $credentials)
    {


        $plain = $credentials['Password'];

        return $this->hasher->check($plain, $user->getAuthPassword());
    }

}
