<?php

namespace App\Http\Requests\Member;

use App\Models\Member;
use Illuminate\Foundation\Http\FormRequest;

class MemberRegisterRequest extends FormRequest
{

    public function messages()
    {
        return [
            'Email.required' => '이메일을 입력해주시기 바랍니다.',
            'Email.unique:user' => '이미 등록 된 이메일 입니다.',
            'Email.min' => '이메일을 최소 6자 이상 입력해주시기 바랍니다.',
            'Email.max' => '이메일을 최소 10자 이상 입력해주시기 바랍니다.',
            'Password.required' => '비밀번호를 입력해주시기 바랍니다.',
            'Password.min' => '비밀번호를 최소 6자 이상 입력해주시기 바랍니다.',
            'Password.max' => '비밀번호를 최대 20자 이하 입력해주시기 바랍니다.'
        ];
    }

    public function attributes()
    {
        return [
            'Name' => '이름',
            'Password' => '비밀번호',
            'Password_c' => '비밀번호 확인',
            'Email' => '이메일',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

      /*  $email = $this->route('Email');

        return Member::where('Email', $email)
            ->where('user_id', Auth::id())
            ->exists();*/
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Email' => 'required|unique|min:6|max:255',
            'Name' => 'required|max:255',
            'Password' => 'required|min:6|max:20|confirmed'
        ];
    }
}
