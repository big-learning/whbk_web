<?php

namespace App\Http\Services\Member;

use App\Http\Services\Service;
use App\Models\MemberDetail;
use App\Models\MemberGroupCategory;
use App\Models\Member;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class MemberService extends Service
{

    //맴버 정보 저장
    public function setMember(array $data){

        $member = Member::create([
            'Name' => $data['Name'],
            'UserType'=> 'NONE',
            'UserLevel'=> 'N',
            'Email' => $data['Email'],
            'Password' => bcrypt($data['Password']),
        ]);

        $token = $this->getMemberToken(['email'=>$data['Email'],'password'=>$data['Password']]);

        return $token;
    }

    //맴버 정보 수정
    public function updateMember($userIdx, array $data){

        $member = Member::find($userIdx);
        if(empty($member)) return false;

        return $member->update($data);
    }

    public function updateMemberDetail($userIdx, array $data){

        return MemberDetail::updateOrCreate(['UserIdx'=>$userIdx],$this->objectNullOut($data));
    }


    //유저 토큰 가져오기
    public function getMemberToken(array $data){

        $http = new Client;
        $response = $http->request('POST',config('app.url').'/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => 'YI6hBXO8KNHB3OCBrWcqUJSNMpnrUbYR915MAeYD',
                'username' => $data['email'],
                'password' => $data['password'],
                'scope' => '*',
            ]
        ]);

        return json_decode((string) $response->getBody(), true);
    }

    //유저 카테고리 가져오기
    public function getCategory($idx){

        $sql = "select CateId01,
                 CateName01,
                 CateId02,
                 CateName02,
                 GroupCateName,
                 GroupCateDescription,
                 userCate.UserIdx
                 from 
                   M_UserGroupCategory as userCate,
                     (
                    select GroupCateIdx as CateId01, GroupCateName as CateName01
                    from SG_GroupCategory
                    where GroupCateLevel = 1 
                 ) as dept1
                 ,
                 (
                    select GroupCateIdx as CateId02, GroupCateName as CateName02, GroupCateParent, GroupCateName, GroupCateDescription
                    from SG_GroupCategory
                    where GroupCateLevel = 2 
                 ) as dept2
                
                 where 1=1
                 and dept1.CateId01 = dept2.GroupCateParent
                 and userCate.GroupCateIdx = dept2.CateId02
                 and userCate.UserIdx = ?";

        $results = DB::select($sql, [ $idx ]);
        return $results;
    }


    //유저 카테고리 저장
    public function setUserGroupCategory($datas){

        MemberGroupCategory::where('UserIdx',$datas['UserIdx'])->delete();

        foreach($datas['GroupCateIdx'] as $groupCateIdx){
            $memberGroupCategory = MemberGroupCategory::create([
                'UserIdx'       => $datas['UserIdx'],
                'GroupCateIdx'  => $groupCateIdx,
            ]);
        }

    }


}