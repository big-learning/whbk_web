<?php

namespace App\Http\Services\Group;

use App\Http\Services\Service;
use App\Models\Group\Group;
use App\Models\Group\GroupCategory;
use Illuminate\Support\Facades\DB;

class GroupCategoryService extends Service
{

    //그룹 카테고리 생성
    public function createCategory($datas = null){

        $isCate = GroupCategory::where('GroupCateParent',$datas['GroupCateParent'])->where('GroupCateName',$datas['GroupCateName']);

        if(empty($isCate)) return false;

        $category = GroupCategory::create([
            'GroupCateParent' => $datas['GroupCateParent'],
            'GroupCateLevel' => $datas['GroupCateLevel'],
            'GroupCateName' => $datas['GroupCateName'],
            'GroupCateDescription' => $datas['GroupCateDescription'],
        ]);

        return $category;
    }

    //그룹 카테고리 업데이트
    public function updateCategory($idx, $datas = null){

        $group = GroupCategory::find($idx);

        if(empty($group)) return false;

        $group->update([
            'GroupCateParent' => $datas['GroupCateParent'],
            'GroupCateLevel' => $datas['GroupCateLevel'],
            'GroupCateName' => $datas['GroupCateName'],
            'GroupCateDescription' => $datas['GroupCateDescription'],
        ]);
        return $group;
    }


    //그룹 카테고리
    public function getCategory($datas = null){

        $where = '';
        if($datas !== null){
            foreach ($datas as $key => $value){
                $where.= " AND ".$key." = '".$value."'";
            }
        }

        $sql = "select CateId01,
                     CateName01,
                     CateId02,
                     CateName02,
                     GroupCateName,
                     GroupCateDescription
                 from 
                     (
                    select GroupCateIdx as CateId01, GroupCateName as CateName01
                    from SG_GroupCategory
                    where GroupCateLevel = 1
                 ) as dept1
                 ,
                 (
                    select GroupCateIdx as CateId02, GroupCateName as CateName02, GroupCateParent, GroupCateName, GroupCateDescription
                    from SG_GroupCategory
                    where GroupCateLevel = 2
                 ) as dept2
                 where 1=1 ".$where." 
                 and dept1.CateId01 = dept2.GroupCateParent";


        $results = DB::select($sql);

        return $results;
    }

    //그룹 유저 삭제
    public function deleteGroupCategory($idx){

        $guser = GroupCategory::find($idx);

        if(empty($guser)) return false;

        return $guser->delete();
    }



}