<?php

namespace App\Http\Services\Group;

use App\Http\Services\Service;
use App\Models\Group\Group;
use Illuminate\Support\Facades\DB;

class GroupScheduleService extends Service
{

    use GroupCommon;

    private $today;

    public function __construct()
    {
        $this->today = date('Ymd');
    }

    //그룹 리스트
    public function playUpdate(){

        DB::table('SG_Group')
            ->where('GroupStartDatetime','<=', $this->today )
            ->where('GroupStatus', 'READY')
            ->update(['GroupStatus' => 'PLAY']);
    }

    public function endUpdate(){
        DB::table('SG_Group')
            ->where('GroupStatus', 'PLAY');
    }


}