<?php
/**
 * Created by PhpStorm.
 * User: SIEE
 * Date: 2017-11-14
 * Time: 오후 2:27
 */

namespace App\Http\Services\Group;


trait GroupCommon
{

    //그룹 카테고리 공통 쿼리
    protected function groupQuery(){
        return "select CateId01,
                     CateName01,
                     CateId02,
                     CateName02,
                     GroupCateName,
                     GroupCateDescription
                 from 
                     (
                    select GroupCateIdx as CateId01, GroupCateName as CateName01
                    from SG_GroupCategory
                    where GroupCateLevel = 1
                 ) as dept1
                 ,
                 (
                    select GroupCateIdx as CateId02, GroupCateName as CateName02, GroupCateParent, GroupCateName, GroupCateDescription
                    from SG_GroupCategory
                    where GroupCateLevel = 2
                 ) as dept2
                 where 1=1
                 and dept1.CateId01 = dept2.GroupCateParent";
    }

}