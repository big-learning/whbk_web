<?php

namespace App\Http\Services\Group;

use App\Http\Services\Service;
use App\Models\Group\GroupUser;
use Illuminate\Support\Facades\DB;

class GroupUserService extends Service
{

    use GroupCommon;

    //유저 그룹 리스트
    public function userGroupList($userIdx){
        $sql = "SELECT 
                  g.GroupIdx,CateId01, CateId02, CateName01, CateName02, GroupLocation, GroupStatus, GroupName, GroupDescription, GroupLimitMembers, GroupStartDatetime, GroupPeriod, gu.UserIdx
                FROM 
                  SG_Group g 
                LEFT JOIN
                  (".$this->groupQuery().") gc ON g.GroupCateIdx = gc.CateId02 
                LEFT JOIN
                  SG_GroupUser gu ON g.GroupIdx = gu.GroupIdx
                WHERE gu.UserIdx = ? ";

        $results = DB::select($sql, [$userIdx]);
        return $results;
    }


    //그룹 유저 생성
    public function createGroupUser($datas = null){

        $isGroupUser = GroupUser::where('UserIdx',$datas['UserIdx'])->where('GroupIdx',$datas['GroupIdx']);

        if(empty($isGroupUser)) return false;

        $guser = GroupUser::create([
            'GroupIdx' => $datas['GroupIdx'],
            'UserIdx' => $datas['UserIdx'],
            'GroupUserLevelIdx' =>  0,
            'GroupUserNickname' => $datas['GroupUserNickname'],
            'GroupUserDescription' => '기본',
            'GroupUserIsAdmin' => !empty($datas['GroupUserIsAdmin']) ? $datas['GroupUserIsAdmin'] : 0
        ]);

        return $guser;
    }


    //그룹 유저 업데이트
    public function updateGroupUser($idx, $datas = null){

        $guser = GroupUser::find($idx);

        if(empty($guser)) return false;

        $guser->update([
            'GroupIdx' => $datas['GroupIdx'],
            'UserIdx' => $datas['UserIdx'],
            'GroupUserLevelIdx' => $datas['GroupUserLevelIdx'],
            'GroupUserNickname' => $datas['GroupUserNickname'],
            'GroupUserDescription' => $datas['GroupUserDescription'],
            'GroupUserIsAdmin' => $datas['GroupUserIsAdmin']
        ]);

        return $guser;
    }


    //그룹 유저 삭제
    public function deleteGroupUser($idx){

        $guser = GroupUser::find($idx);

        if(empty($guser)) return false;

        return $guser->delete();
    }

}