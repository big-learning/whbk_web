<?php

namespace App\Http\Services\Group;

use App\Http\Services\Service;
use App\Models\Group\Group;
use Illuminate\Support\Facades\DB;

class GroupService extends Service
{

    use GroupCommon;

    //그룹 리스트
    public function listGroup($search = '',$limit = null){

        $sql = "SELECT 
                  GroupIdx,CateId01, CateId02, CateName01, CateName02, GroupLocation, GroupStatus, GroupName, GroupDescription, GroupLimitMembers, GroupStartDatetime, GroupPeriod, UserIdx
                FROM 
                  SG_Group g 
                LEFT JOIN
                  (".$this->groupQuery().") gc ON g.GroupCateIdx = gc.CateId02 
                WHERE 1=1 ".($search ? " AND ( g.GroupName like '%".$search."%' or g.GroupDescription like '%".$search."%' ) " : " " ).
                ($limit !== null ? " LiMIT ?, ?":"");


        $results = DB::select($sql, $limit);
        return $results;
    }


    //그룹 생성
    public function createGroup($datas = null){

        /*$isGroup = Group::where('GroupName',$datas['GroupName']);

        if(empty($isGroup)) return false;*/

        $group = Group::create([
            'GroupCateIdx' => $datas['GroupCateIdx'],
            'GroupLocation' => $datas['GroupLocation'],
            'GroupStatus' => $datas['GroupStatus'],
            'GroupName' => $datas['GroupName'],
            'GroupDescription' => $datas['GroupDescription'],
            'GroupLimitMembers' => $datas['GroupLimitMembers'],
            'GroupStartDatetime' => str_replace('-','',$datas['GroupStartDatetime']),
            'GroupPeriod' => $datas['GroupPeriod'],
            'UserIdx' => $datas['UserIdx']
        ]);

        return $group;
    }

    //그룹 업데이트
    public function updateGroup($idx, $datas = null){

        $group = Group::find($idx);

        if(empty($group)) return false;

        $group->update([
            'GroupCateIdx' => $datas['GroupCateIdx'],
            'GroupLocation' => $datas['GroupLocation'],
            'GroupStatus' => $datas['GroupStatus'],
            'GroupName' => $datas['GroupName'],
            'GroupDescription' => $datas['GroupDescription'],
            'GroupLimitMembers' => $datas['GroupLimitMembers'],
            'GroupStartDatetime' => $datas['GroupStartDatetime'],
            'GroupPeriod' => $datas['GroupPeriod'],
            'UserIdx' => $datas['UserIdx']
        ]);
        return $group;
    }


    //전체 카테고리 가져오기
    public function getCategory($options = null){

        $sql = $this->groupQuery();

        $results = DB::select($sql);

        return $results;
    }

    //그룹 유저 삭제
    public function deleteGroup($idx){

        $guser = Group::find($idx);

        if(empty($guser)) return false;

        return $guser->delete();
    }



}