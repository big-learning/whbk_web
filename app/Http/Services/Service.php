<?php

namespace App\Http\Services;

use http\Exception\BadMethodCallException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Service
{


    public function callAction($method, $parameters)
    {
        return call_user_func_array([$this, $method], $parameters);
    }

    public function __call($method, $parameters)
    {
        throw new BadMethodCallException("Method [{$method}] does not exist.");
    }


    function convertWhereText($whereArray){

        $whereText = '';
        foreach($whereArray as $key => $value){
            $whereText.=  (!empty($whereText) ? ' AND ':'').$key.' = "'.$value.'"';
        }

        return $whereText;
    }


    function objectNullOut(array &$object){
        foreach ($object as $key => $value){
            if($value == '') unset($object[$key]);
        }
        return $object;
    }
}
