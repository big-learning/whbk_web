<?php

namespace App\Http\Controllers\Api\Group;

use App\Commons\ListManagement;
use App\Http\Controllers\Controller;
use App\Http\Services\Group\GroupService;
use App\Http\Services\Group\GroupUserService;
use Illuminate\Http\Request;

class GroupController extends Controller
{

    use ListManagement;

    private $groupService;
    private $groupUserService;


    public function __construct(
        GroupService $groupService,
        GroupUserService $groupUserService
    ){

        $this->groupService = $groupService;
        $this->groupUserService = $groupUserService;
    }


    //그룹 카테고리 전체
    public function category(){

        $category = $this->groupService->getCategory();

        return response()->json([
            'category'=>$category
        ]);
    }

    //그룹 리스트
    public function allList(Request $request){

        $params = $request->all();
        $params['limit'] = !empty($params['limit']) ? $params['limit'] : 10;
        $params['offset'] = !empty($params['offset']) ? $params['offset'] : 1;
        $text = !empty($params['text']) ? $params['text'] : '';
        $limit = $this->limit($params['offset'], $params['limit']);
        $list = $this->groupService->listGroup($text, $limit);

        return response()->json([
            'list'=>$list,
            'limit'=> $limit
        ]);
    }

    //유저 그룹 리스트
    public function userList($idx, Request $request){

        $list = $this->groupUserService->userGroupList($idx);

        return response()->json([
            'list'=>$list,
        ]);
    }


    //그룹 생성
    public function create(Request $request){

        $params = $request->post();
        $params['GroupStatus'] = 'READY';

        $group = $this->groupService->createGroup($params);

        if($group){
            //그룹 유저 저장
            $params['GroupUserNickname'] = $params['Name'];
            $params['GroupIdx'] = $group['GroupIdx'];
            $params['GroupUserIsAdmin'] = 1;

            $this->groupUserService->createGroupUser($params);
        }else{
            return response()->json([
                'status'=>'fail',
                'massage'=>''
            ],404);
        }

        return response()->json([
            'status'=>'success',
            'group' => $group
        ]);
    }

    //그룹 수정
    public function update(Request $request){
        $group = $this->groupService->updateGroup($request->post('GroupIdx'),$request->post());

        return response()->json([
            'status'=>'success',
            'group' => $group
        ]);
    }

    //그룹 삭제
    public function delete(Request $request){

        $group = $this->groupService->deleteGroup($request->post('GroupIdx'));

        return response()->json([
            'status'=>'success'
        ]);
    }
}
