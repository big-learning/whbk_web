<?php

namespace App\Http\Controllers\Api\Member;

use App\Http\Services\Member\MemberService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller
{

    private $memberService;

    public function __construct(MemberService $memberService)
    {
        $this->memberService = $memberService;
    }

    //유저 회원가입
    public function register(Request $request)
    {
        $vaildate = $this->validator($request->post());
        if($vaildate->fails()){
            return response()->json(['massage'=>$vaildate->errors()->first()], 401);
        }

        $token = $this->memberService->setMember($request->post());

        //$member->Email;
        //$member->Name;

        return response()->json([
                'massage'=>'회원가입이 완료 되었습니다.',
                'token'=>$token
        ]);
    }
    

    //유저 업데이트
    public function update($idx, Request $request){

       $params = $request;
       $result = $this->memberService->updateMember($idx, ['Name' =>$params['Name']]);

       $detail = [
           'Sex' => $params['Sex'],
           'Tel' => $params['Tel'],
           'Birth' => str_replace('-','',$params['Birth']),
           'Location' => $params['Location'],
           'Info' => $params['Info'],
       ];
       $result = $this->memberService->updateMemberDetail($idx,$detail);

        return response()->json([
            'massage'=>'회원정보가 수정되었습니다.',
        ]);
    }

    public function userPhotoUpload(Request $request){
        $file = $request->file('image') ;
        if ($file->isValid()) {
            //
        }
        $path = $request->image->path();

        $extension = $request->image->extension();

        //$path = $request->image->storeAs('image', 'filename.jpg');
    }


    //유저 관심 카테고리 가져오기
    public function category($idx, Request $request){

        $category = $this->memberService->getCategory($idx);

        return response()->json([
            'category'=>$category
        ]);
        
    }

    //유저 관심 카테고리 저장
    public function setCategory(Request $request){

        $idx = $request->get('idx');

        $this->memberService->setUserGroupCategory([
            'UserIdx' => $request->get('userIdx'),
            'GroupCateIdx' => $request->get('groupCateIdx')
        ]);
        return response()->json([
            'status'=>'success'
        ]);
    }

    //유저 삭제
    public function delete(Request $request)
    {

    }


    private function validator(array $data)
    {
        return Validator::make($data, [
            'Email' => 'required|unique:M_User|email|min:6|max:255',
            'Name' => 'required|max:255',
            'Password' => 'required|min:6|max:20|confirmed'
        ],[],[
            'Name' => '이름',
            'Password' => '비밀번호',
            'Password_confirmation' => '비밀번호 확인',
            'Email' => '이메일',
        ]);
    }


}
