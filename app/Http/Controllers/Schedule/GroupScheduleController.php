<?php

namespace App\Http\Controllers\Schedule;

use App\Commons\ListManagement;
use App\Http\Controllers\Controller;
use App\Http\Services\Group\GroupScheduleService;
use Illuminate\Support\Facades\Log;

class GroupScheduleController extends Controller
{

    use ListManagement;

    private $groupScheduleService;


    public function __construct(
        GroupScheduleService $groupScheduleService
    ){

        $this->groupScheduleService = $groupScheduleService;
    }


    //그룹 시작 되지 않은 스터디 시작
    public function playStudy(){
        $this->groupScheduleService->playUpdate();
    }


    //스터디 종료 시키기
    public function endStudy(){
        $this->groupScheduleService->endUpdate();
    }

}
