<?php
namespace App\Commons;

trait ListManagement
{

    function ArrayList($iterable, callable $f)
    {
        foreach ($iterable as $value) {
            yield $f($value);
        }
    }

    function limit($offset = 1,$limit){
        $max = $limit * $offset;
        $min = $max - $limit;
        $max = $max -1;
        return [$min, $limit];
    }
}