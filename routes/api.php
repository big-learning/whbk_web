<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/register', 'Api\Member\MemberController@register')->name('api.register');

//유저
Route::put('/user/{idx}', 'Api\Member\MemberController@update')->name('api.user.update');
Route::post('/user/{idx}/photo', 'Api\Member\MemberController@userPhotoUpload')->name('api.user.image');


Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
});


//그룹
Route::get('/category/', 'Api\Group\GroupController@category')->name('api.group.cate');

Route::get('/group', 'Api\Group\GroupController@allList')->name('api.group.list');
Route::get('/group/{idx}', 'Api\Group\GroupController@view')->name('api.group.view');
Route::post('/group', 'Api\Group\GroupController@create')->name('api.group.create');
Route::put('/group/{idx}', 'Api\Group\GroupController@update')->name('api.group.update');

//유저 그룹 리스트
Route::get('/group/user/{idx}',       'Api\Group\GroupController@userList')->name('api.user.group.list');

//그룹 카테고리
Route::get('/group/{idx}/category/{cateidx}', 'Api\Group\GroupController@group')->name('api.group.cate.idx');

//유저 카테고리
Route::get('/user/category/{idx}', 'Api\Member\MemberController@category')->name('api.user.cate.view');
Route::post('/user/category',       'Api\Member\MemberController@setCategory')->name('api.user.cate.save');





