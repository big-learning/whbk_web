<?php

return [


    'CLIENT_ID' => env('API_CLIENT_ID','2'),

    'CLIENT_SECRET' => env('API_CLIENT_SECRET','j7kb5BfUCwgbnqfXRIle6BwY4VBYrWaZRI7DQI6T'),


    //
    'TYPE' => [
        'NONE' => '기본',
        'HOT'  => '최근 기여 높은 고객',
        'LONG' => '장기 충성 고객',
        'REST' => '최근 접속 하지 않는 유저',
    ],


    'LEVEL' => [
        'N' => '기본',
        'C' => '10만원이상',
        'B' => '30만원이상',
        'A' => '50만원이상',
        'S' => '100만원이상',
        'SS'=> '500만원이상',
        'SSS'=> '1000만원이상'
    ]

];
