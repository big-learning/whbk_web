<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('SG_GroupCategory')) {
            Schema::create('SG_GroupCategory', function (Blueprint $table) {

                $table->increments('GroupCateIdx');

                $table->integer('GroupCateParent')->unsigned()->default(0);
                $table->integer('GroupCateLevel')->unsigned()->default(0);

                $table->string('GroupCateName', 128);
                $table->string('GroupCateDescription', 255);

                $table->timestamp('UpdateDatetime')->nullable();
                $table->timestamp('CreateDatetime')->nullable();
            });
        }

        if (!Schema::hasTable('SG_GroupKeyword')) {
            Schema::create('SG_GroupKeyword', function (Blueprint $table) {

                $table->integer('GroupIdx')->unsigned();

                $table->string('GroupKeyword', 128);
                $table->string('GroupKeywordDescription', 255);

                $table->timestamp('UpdateDatetime')->nullable();
                $table->timestamp('CreateDatetime')->nullable();

                $table->primary(['GroupKeyword', 'GroupIdx']);
            });
        }

        if (!Schema::hasTable('SG_GroupUser')) {
            Schema::create('SG_GroupUser', function (Blueprint $table) {

                $table->increments('GroupUserIdx');

                $table->integer('GroupIdx')->unsigned();
                $table->integer('UserIdx')->unsigned();
                $table->integer('GroupUserLevelIdx')->unsigned();

                $table->string('GroupUserNickname', 128);
                $table->string('GroupUserDescription', 255);

                $table->boolean('GroupUserIsAdmin')->default(false);

                $table->timestamp('UpdateDatetime')->nullable();
                $table->timestamp('CreateDatetime')->nullable();

                $table->primary(['GroupIdx', 'UserIdx']);
            });
        }

        if (!Schema::hasTable('SG_GroupUserLevel')) {
            Schema::create('SG_GroupUserLevel', function (Blueprint $table) {

                $table->increments('GroupUserLevelIdx');

                $table->integer('GroupIdx')->unsigned();
                $table->string('GroupUserLevelName', 150);
                $table->string('GroupUserLevelDescription', 255);

                $table->text('GroupUserLevelAuthority');

                $table->integer('UserIdx')->unsigned();
                $table->timestamp('UpdateDatetime')->nullable();
                $table->timestamp('CreateDatetime')->nullable();
            });
        }

        if (!Schema::hasTable('SG_Group')) {
            Schema::create('SG_Group', function (Blueprint $table) {

                $table->increments('GroupIdx');
                $table->integer('GroupCateIdx')->unsigned();

                $table->string('GroupLocation', 150);
                $table->string('GroupStatus', 30);
                $table->string('GroupName', 128);
                $table->text('GroupDescription');
                $table->tinyInteger('GroupLimitMembers');

                $table->string('GroupStartDatetime', 8);
                $table->tinyInteger('GroupPeriod')->default(0);

                $table->integer('UserIdx')->unsigned();
                $table->timestamp('UpdateDatetime')->nullable();
                $table->timestamp('CreateDatetime')->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('SG_Group')) {
            Schema::drop('SG_GroupCategory');
            Schema::drop('SG_GroupKeyword');
            Schema::drop('SG_GroupUser');
            Schema::drop('SG_GroupUserLevel');
            Schema::drop('SG_Group');
        }
    }
}
