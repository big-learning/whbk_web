<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserCateGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('M_UserGroupCategory')) {
            Schema::create('M_UserGroupCategory', function (Blueprint $table) {
                $table->integer('UserIdx')->unsigned();
                $table->integer('GroupCateIdx')->unsigned();
                $table->timestamp('CreateDatetime')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('M_UserGroupCategory');
    }
}
