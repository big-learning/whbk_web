<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('M_Level', function (Blueprint $table) {

            $table->string('UserLevel')->nullable();
            $table->string('UserLevelName')->nullable();

            $table->primary('UserLevel');
        });

        Schema::create('M_Type', function (Blueprint $table) {

            $table->string('UserType')->nullable();
            $table->string('UserTypeName')->nullable();

            $table->primary('UserType');
        });


        Schema::create('M_User', function (Blueprint $table) {
            $table->increments('UserIdx');
            $table->enum('UserLevel',['N','D','C','B','A','S','SS','SSS'])->unllable()->default('D');
            $table->string('UserType',20);
            $table->string('part', 20)->default('D');
            $table->string('Email')->unique()->nullable();
            $table->string('Name')->nullable();
            $table->string('Password');
            $table->string('RememberToken', 100)->nullable();
            $table->timestamp('LastDatetime')->nullable();
            $table->timestamp('UpdateDatetime')->nullable();
            $table->timestamp('CreateDatetime')->nullable();

            $table->index('Email', 'my_index_email');
        });



        Schema::create('M_UserDetail', function (Blueprint $table) {
            $table->integer('UserIdx')->unsigned();
            $table->integer('Sex');
            $table->string('Tel',15);
            $table->string('Birth',6);
            $table->string('Zipcode',30);
            $table->string('Address',150);
            $table->string('AddressDetail',255);

            $table->primary('UserIdx');
            $table->foreign('UserIdx')->references('UserIdx')->on('M_User')->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('M_Level');
        Schema::drop('M_Type');
        Schema::drop('M_UserDetail');
        Schema::drop('M_User');
    }
}
