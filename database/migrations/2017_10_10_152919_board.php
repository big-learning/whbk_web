<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Board extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('CM_BoardGroup')) {
            Schema::create('CM_BoardGroup', function (Blueprint $table) {

                $table->increments('BoardGroupIdx')->unsigned();
                $table->string('BoardGroupId', 32);
                $table->string('BoardGroupName', 128);
                $table->string('BoardGroupCate', 30);
                $table->string('BoardGroupDescription', 128);
                $table->string('BoardGroupType');

                $table->integer('BoardTotalRow')->unsigned()->default(0);

                $table->timestamp('UpdateDatetime')->nullable();
                $table->timestamp('CreateDatetime')->nullable();

                $table->primary('BoardGroupId');
            });
        }


        if (!Schema::hasTable('CM_Board')) {
            Schema::create('CM_Board', function (Blueprint $table) {

                //$table->string('BoardGroupId',32);
                $table->increments('BoardIdx');

                $table->integer('BoardParentIdx')->unsigned();
                $table->integer('BoardDept')->unsigned();

                $table->tinyInteger('BoardNotice')->default(0);
                $table->tinyInteger('BoardBest')->default(0);
                $table->tinyInteger('BoardHide')->default(0);

                $table->string('BoardTitle', 255);
                $table->text('BoardContent');

                $table->integer('UserIdx')->unsigned();
                $table->integer('BoardHit')->unsigned();

                $table->string('TMP1', 128);
                $table->string('TMP2', 128);
                $table->string('TMP3', 128);
                $table->string('TMP4', 128);
                $table->string('TMP5', 128);

                $table->timestamp('UpdateDatetime')->nullable();
                $table->timestamp('CreateDatetime')->nullable();
            });
        }

       /* Schema::create('CM_BoardOption', function (Blueprint $table) {

            $table->integer('BoardIdx')->unsigned();
        });*/
        if (!Schema::hasTable('CM_Reply')) {
            Schema::create('CM_Reply', function (Blueprint $table) {

                $table->increments('ReplyIdx');

                //$table->string('BoardGroupId',32);
                $table->integer('BoardIdx')->unsigned();

                $table->tinyInteger('ReplyBest')->default(0);
                $table->tinyInteger('ReplyHide')->default(0);

                $table->text('ReplyContent');

                $table->string('TMP1', 128);
                $table->string('TMP2', 128);
                $table->string('TMP3', 128);
                $table->string('TMP4', 128);
                $table->string('TMP5', 128);

                $table->integer('UserIdx')->unsigned();
                $table->timestamp('UpdateDatetime')->nullable();
                $table->timestamp('CreateDatetime')->nullable();
            });
        }

        /*Schema::create('CM_ReplyOption', function (Blueprint $table) {

            $table->integer('ReplyIdx')->unsigned();
        });*/

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        //Schema::drop('CM_ReplyOption');
        Schema::drop('CM_Reply');
        //Schema::drop('CM_BoardOption');
        Schema::drop('CM_Board');
        Schema::drop('CM_BoardGroup');
    }
}
