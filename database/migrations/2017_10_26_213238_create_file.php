<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('SYS_File')) {
            Schema::create('SYS_File', function (Blueprint $table) {

                $table->increments('FileIdx');

                $table->string('FileTable', 128);
                $table->integer('FileTablePK')->unsigned();
                $table->enum('FileType', ['IMG', 'FILE']);

                $table->string('FilePath', 128);
                $table->string('FileName', 128);
                $table->string('FileOriginName', 128);
                $table->string('FileContent', 32);
                $table->integer('FileSize')->unsigned();

                $table->integer('UserIdx')->unsigned();
                $table->timestamp('UpdateDatetime')->nullable();
                $table->timestamp('CreateDatetime')->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('SYS_File')) {
            Schema::drop('SYS_File');
        }
    }
}
