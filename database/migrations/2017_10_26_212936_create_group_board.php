<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupBoard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('SG_GroupBoard')) {
            Schema::create('SG_GroupBoard', function (Blueprint $table) {

                $table->increments('GroupBoardIdx');

                $table->integer('GroupIdx')->unsigned();
                $table->tinyInteger('GroupBoardNotice')->default(0);

                $table->string('GroupBoardTitle', 128);
                $table->text('GroupBoardContent');

                $table->string('TMP1', 128);
                $table->string('TMP2', 128);
                $table->string('TMP3', 128);
                $table->string('TMP4', 128);
                $table->string('TMP5', 128);

                $table->integer('UserIdx')->unsigned();
                $table->timestamp('UpdateDatetime')->nullable();
                $table->timestamp('CreateDatetime')->nullable();
            });
        }
        if (!Schema::hasTable('SG_GroupReply')) {
            Schema::create('SG_GroupReply', function (Blueprint $table) {

                $table->increments('GroupReplyIdx');

                $table->integer('GroupIdx')->unsigned();
                $table->integer('GroupBoardIdx')->unsigned();

                $table->text('GroupBoardReplyContent');

                $table->string('TMP1', 128);
                $table->string('TMP2', 128);
                $table->string('TMP3', 128);
                $table->string('TMP4', 128);
                $table->string('TMP5', 128);

                $table->integer('UserIdx')->unsigned();
                $table->timestamp('UpdateDatetime')->nullable();
                $table->timestamp('CreateDatetime')->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('SG_GroupBoard')) {
            Schema::drop('SG_GroupBoard');
            Schema::drop('SG_GroupReply');
        }
    }
}
